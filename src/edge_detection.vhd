----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/01/2019 10:56:39 AM
-- Design Name: 
-- Module Name: edge_detection - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity edge_detection is
    Port ( clk : in STD_LOGIC;
           I : in STD_LOGIC;
           redge : out STD_LOGIC;
           fedge : out STD_LOGIC;
           transistion : out STD_LOGIC);
end edge_detection;

architecture Behavioral of edge_detection is

signal I_d  : std_logic := '0';  -- input delayed by 1 cycle


begin
    
    I_d <= I when rising_edge(clk);
    process (clk)
    begin
        if (rising_edge(clk)) then
            redge <= not I_d and I;  -- old = 0, new = 1 => rising edge
            fedge <= I_d and not I;  -- old = 1, new = 0 => falling edge
            transistion <= I_d xor I;      -- old <> new       => changed
        end if;
    end process;
end Behavioral;
