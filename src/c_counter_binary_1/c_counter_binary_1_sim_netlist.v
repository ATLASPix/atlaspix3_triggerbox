// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2700185 Thu Oct 24 18:45:48 MDT 2019
// Date        : Thu Nov 14 15:31:49 2019
// Host        : pc-unige-peary running 64-bit Ubuntu 16.04.6 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/peary/ATLASPix3-peary/ATLASPix3FW/ATLASPixIPs/atlaspix3_triggerbox/src/c_counter_binary_1/c_counter_binary_1_sim_netlist.v
// Design      : c_counter_binary_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z045ffg900-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "c_counter_binary_1,c_counter_binary_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_counter_binary_v12_0_14,Vivado 2019.2" *) 
(* NotValidForBitStream *)
module c_counter_binary_1
   (CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [47:0]Q;

  wire CLK;
  wire [47:0]Q;
  wire SCLR;
  wire NLW_U0_THRESH0_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "48" *) 
  (* C_XDEVICEFAMILY = "zynq" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "1" *) 
  (* c_has_load = "0" *) 
  (* c_has_thresh0 = "0" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "0" *) 
  (* c_thresh0_value = "1" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  c_counter_binary_1_c_counter_binary_v12_0_14 U0
       (.CE(1'b1),
        .CLK(CLK),
        .L({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .LOAD(1'b0),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(NLW_U0_THRESH0_UNCONNECTED),
        .UP(1'b1));
endmodule

(* C_AINIT_VAL = "0" *) (* C_CE_OVERRIDES_SYNC = "0" *) (* C_COUNT_BY = "1" *) 
(* C_COUNT_MODE = "0" *) (* C_COUNT_TO = "1" *) (* C_FB_LATENCY = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_LOAD = "0" *) (* C_HAS_SCLR = "1" *) 
(* C_HAS_SINIT = "0" *) (* C_HAS_SSET = "0" *) (* C_HAS_THRESH0 = "0" *) 
(* C_IMPLEMENTATION = "0" *) (* C_LATENCY = "1" *) (* C_LOAD_LOW = "0" *) 
(* C_RESTRICT_COUNT = "0" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_THRESH0_VALUE = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "48" *) 
(* C_XDEVICEFAMILY = "zynq" *) (* ORIG_REF_NAME = "c_counter_binary_v12_0_14" *) (* downgradeipidentifiedwarnings = "yes" *) 
module c_counter_binary_1_c_counter_binary_v12_0_14
   (CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    UP,
    LOAD,
    L,
    THRESH0,
    Q);
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  input UP;
  input LOAD;
  input [47:0]L;
  output THRESH0;
  output [47:0]Q;

  wire \<const1> ;
  wire CLK;
  wire [47:0]Q;
  wire SCLR;
  wire NLW_i_synth_THRESH0_UNCONNECTED;

  assign THRESH0 = \<const1> ;
  VCC VCC
       (.P(\<const1> ));
  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "48" *) 
  (* C_XDEVICEFAMILY = "zynq" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "1" *) 
  (* c_has_load = "0" *) 
  (* c_has_thresh0 = "0" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "0" *) 
  (* c_thresh0_value = "1" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  c_counter_binary_1_c_counter_binary_v12_0_14_viv i_synth
       (.CE(1'b0),
        .CLK(CLK),
        .L({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .LOAD(1'b0),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(NLW_i_synth_THRESH0_UNCONNECTED),
        .UP(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
ETPSkHr704UHxotu/+Q540l9cX8jPkemopoZFBrfLcZwZSqWsAYNlYOxA+8clWO0IaIobvUTExwu
3qiDkUim7Q==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
q/ZujnFSVQXVdMiB7t3RgoKgod0SiCI7cie3Bajj7ZUMRH21cn/WoEmh+i2/U5nvJ1IxjHZMMsnL
jNOgnuRl/uz6gmv2hslUt+TdGzhMaPPve9oPXv8EmhInBh/UlGQD0X3tVrbYw8LdxKhblhrQN1KD
WgzGpvXYCfHOlJd77ss=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
ETTzQTvFs8K5urlNIkSEqcCqQsRVMALSpC2Jhf3M591IpL4n8eCCrlWEDqdxRPk61veqcN2MuIQQ
RD0BSRDwbe9cjxrZEgwtxzD35fBocQFb/DkEfpxz5nyKxyzet91/0nPIanDRa8GGanUSKmYlKU4t
DjmUodKGe1PJ784R5WQejJ9XyRiCuptKO0ruupgSlwSrkLGp0E2QovN0UbLsrFzrGMgmTybR6mHa
JJNAosTk8n3+uVLbFJxQJ2nGrdJtMdUisHnNuYeEXpxpP1Sj7G0YPNIk4646/cKcNHUpiFM2YMbG
H4nXuh5WxhHxYXB4gxvuI63CQL6DqNIXvgI4lg==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
dkpnsBBbY7QGv6Hkw1rgHmViq/T7Sx0Hh3rXk6KnFSL0hzEQJXpVJi7OYdCcfQWt4K95/pt9Xqav
t6NybSZdaZ+tgnwyGHZMk3dJwxIprrCl+VecY2pLm4qimW8XAxa6FuxvhJiSc4VfyMGXJKF4WKO8
GjcBAGFd25XQH0coBhoBujfpK7hmVkqkrN6XLtiAC9F6H3BtRSiQZ0zhEf+kYQ0dS60WDkFNnsOn
liCqc/kNkD5k76q/z2vyIsWUbUxOa2HbbmoW1u8G7PpiY0bbd2+gzAzVb8XQtqbyQjZvHZE13dyg
OSi9FQgiTvFQeYzSrZg0cMfmPdeZ0ILephLH5A==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
ZBMTxFJ+tGZo+4Sjh6eizbFlkO4UaKOo0Ngso0uimVhzqV/QfS3JpX4ZBNhbOJMFUaOr3lEUjTSm
f8AtNwP9rzu5gRbP+L7TElZLp30fQimeDNpzzg2v337qX7GEHe7tsISCFLaIJDerXU+G06e3RbvW
NgVY58cpi8Ms/TFeK5AipVNgtZf7eMV1oD4jRlEBqcKotH6cchebYiWkCM9Vb60MNcVmVl2zRd2T
NJNwwqf/Kz0ks5E9GY+ZFGZJP4D30RPJqHgrbx5+68s19m/3OZxUEJijPmffMk4AQRhppyEEav/y
0avcDCYIMXYbPCB7pysSRoFl0CKSz3LJQz6Ong==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
EL6gzAbRUo7MBxcCtAoaECEuRnI1Q2C00UaSwLXL1ryLpnokfTPoB/bgKtfKkh+MzFzPW6ah8YfI
veVoldKK9xXCPTwJNfmaG8+M3rGpKC9pSEdaFHwaxzVKMG5jcKT0qCK7lmB/2FnMhinVhL2z8vPP
p5226h5MrC6lk2zcuI4=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
G5HcEILPkBkrQPoq+r9Sj5tP/XOXd86ycmkB2y9avVO5e0vj49/0a0iiLBe/kpLlEY3elZA8xZ59
6KTr10Nmww8L8pwcutMWrv3j7vmH9wL5kqC9m9X6IUEb6N4Qdc7kxN31QBzGNWAyg6TYXXFe+QD6
n9kejOyxlfnVMAIv0zM45mQLpCMagDuFcOXPkPnuJv61D3Jj0X43Kt1MUwkq1HKYHib11MQ3H54b
mfDxwUYly6kku1iVCT07iWEPGhg4IndboQUCbq5L/y0ysKXQASfI0/m8mB6doIxTGk78cNHPckHc
a2aspT/J9xDfQaSP38NLIcmmfMuRhYHkZweLcg==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
fRFPk71NAOXOZBIEWKpRRE3o56Tgt1tv9LQ8x/bDyDwg7szh8uIzHE2N55+0WrGlxdKxvPtRWLF3
X26CwZ4sU8Pvh2GNxlW91WKid1joKXXZ1qQt59o6adD8kzDFSj01HYU3of1+Z1LH67iRT4l780QA
KX7MSQsrG8viINk1AfCUOH8IVGPWZwlsuP4dqoaTU+UXdVv07pU9nEu+/30rolEZFGSL9OicMRhQ
OWdKxtfmiVdZS17j56HN9cvko5CBgVGTuVZk6ZvkcOnu9BGhWAcjElkXjs6YvgqxTVfEqnrsWGvu
ORNBZkiwjq0diUtt47hTU43xUD7/B4I1GZZAGw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
qoc3UFplXpY2Umt9dSbc4gszOWJOFWkWt5PYPXXxCrO4LLdHgGZZtmSC+2MDkZ+zJ/BZ5n73MjTg
jseLacEABx+wa0O8ZjYv8xpwTguch/msPGHzNh7GuF2SoPbkqLKEzUZ8VpjkL65jfh75uck9j4s5
MCa+6kCsNmLE7+OQFhvZD7UhSjmRTPM/4Pm2b/UkT3OYXXRVg1kV0tanc8YyzMJlaY/XEK+QZH/0
47CL7cYz1eWWj3OUvSLdPA4ni5FK9uVj0XQ0hw4ZsVmiLwde+E0mzQqFbiw+hBIarjj7/pSOiJQX
8WFfZYwD6tl1iLVvgPe04OXBEDOhqN7WisS3eA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
WGqc3HCJoQi59r4uCCqCs8uIbTOEjbzB2xTM/u99LSX15k7WBRnoGHoeAHH2gUeOcl2awXjj3d+c
NX+GnAoGtskdzhEDm5O+g+0PnDycUaYG2dcgOISp70yKA7IE6JEVIwy6StwenXtka8svY6X+XLrP
gVYER21fKHHn3lO8vmQR9zuhxXICFw/gn1vrKcM3YAcMi0xPFaOHFn7vdzcaJo/u12dMdzIFK7NJ
ltPiOOoP6Yeolsxs9Bg9/PHTSB1CFtcq5Vx+UPW+bkKMcbCQFqj7i0Aek4I9BnnAtPsEnKNIRmlU
Pw7M2Vwvcl1dlvAUICfeZYq21CoTW80WrE7jgg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 25056)
`pragma protect data_block
IwteU/NcvSUIWEpyYwkTPQvTxCv9xllha8A++wTSGi/UTmkdetOpZ9GYGFGoeeb8ZwtrUSwuyEfT
6VPJehrKXRHD65dYR0R/5ZGd8EVHMa6AUplEJLri6Vk86j68z1yu8ALG1U68ZyHQS+FDow29vWZp
iNte1J7hBnOvNGc0DCvvOT/kDW6mHdptfSbLgb5IXWsAJbmgPcOnkj1Ig3WGbsxe3YgHXjt6/Mc4
4V/7Q3ntRYs6WbQQd1FGGhppN31PoDGyuNQBlPkRBlYi9j4Jf3tjEDiYWak1G3JvpD/8YjOMg5Ek
jY3jaSpTsR6teoOF6Alv8uh1Xtf6rT8wjaMZk9LxZvlHsCsNVrlDxo6lWx7pTxUvvxSBEpXzJJcB
ZzMVqVBRYIu0gNQWdqnLsyLQL1ye3nN+F6f2rZ+RswxvjHwibMawMyzyAoHLSqR61Y3B0OvaZxH/
MGBnumH4yo8JtO9xCCUN1OPesPL5eWR2xGT70Mn6kXmA6l0eJyGpz35LJz1EF2keKB6Vj3Jbhmk0
iUASulvkO/ndrPEP1GY/rCk8/J05FT/tvEdpPZ1XMNzu6tHdCPRWIG2up6xEDMcVYkQUCFOVFO/s
CP/E9mSrq4tcY6XXf+G4mE4yEVpLgn1Uiuwr7W+kcdoY1xJPSMsIWJG+amFLoXhADBw+57f0euXv
GTkOLca+5vSKrboo767HNPbE8c3nLYZWqzOmn6Ik4RJPIcZbOJtyXJXScJSw8+eua+q3e/nuBQhf
cAPHmdJD1VZgtACDbqc6nAtgF8Ps7TrTTA3ruezjLJPr4ysvMj5pOoa/82aOdQGQhLys6t9/xVnU
ytNCNLJYiYBl5IpjzCVSJoCI/o81xtd0B+D3XLP7KR0v05Cip+m2kn49QiNfCmdh5i3gQ7X5GhIx
49JlpSClNWRWw1iq+Fy4I/tH8yMitHSa/o7J1QpsnA6YfI4wxJsJPfHqBtyHfruJTK0iHoExWvRF
2uoEoqysxsty7FmAPOSMb5oAYZDpfGs5MOvBxN05V2q+bwwMi5MJJuLxoHSJwi/UDrnNN6PQSJbO
Ms0cIvV/Z/JzJ6Dr3XovDu8tOS6KGpImeL/4K8bfjyORIM7wiRqA8lziVJpZ9J8MZM2WsieSAmLG
N3t8TR1P/lM1R+R4bfAmkSJt1mdKwXxIAWAy7xcDRANcAkyFAkUt8UbXoD8+BEb2D2Xe1Sb1wvLS
lw4VOVE4JUGdfVeub4SK9UqXq+RFBCgQ5oJ1kyzJGXegJ2yt7lUNiE8OEHp28R+bRkfdLZCQY0qh
mhvPVLeihgQAtvz4bs1clWyWcEoV5vw0d9wrKtlNij4872XdjfjBDhfECMssQz4vuDgvMgYvA1wp
bLapHArAS3IIhkwxvVAQNA4DxHNDAOCJq5BABbfZjrcv+45TvxqrhNKi6xCQW+6QospsZvyaQ2o4
YrSP8tVa6DjZlxhMJt1sasPvFqP8ursVwCF1kTbztfxditCPthXq4FpOEtEwAaB1NWXDqA/V/sVf
L6bedXx898Pdcu7lyyoq9ir+sI9zo9fzw1UK+JJn8yOejUqMRVA2Mx6Gll5f0OMjgko6dx37q4b2
7NaiRDdb4DZ3lU/tGMcTDax+gy7T3YwhBnGXaS2r2RumxaNLPEkojhRj3btJuc7L7SQE0xlHOtGU
levI3CBCqptzCdODFD8oe5OeMM46kYx15QzcccUmaiLiEbO+kvU45MeJ8n1V9Kbdpmhgdmrzevr2
+CtNKSAce+VmvmdjRj0+uVckXJFjglhiQYiaXqX1G8lfSeNIh7dhWdZ12dCbWbhJlorwkjpHS1yA
SgZKKj+5qTFUevW/j3SGHMXVufy3n6d4fgK7P0uY1b3tnyT7Eko44kV20xEJZh0+WXZA2XalX63A
7B7TdjiuGPmydoQoW+mppFTl57bkBU8xcmhX85vjOGCmAUQzFgVdnFswCTr1DAgQ/vyFUGPItd5K
0Svhw/YI3PTFsxZUn6vnw6kzJ/722jRjOmSAQoECcbX2eEqExTKLjzNByppFMzTpRBrmMkCrkPfz
M9/YO0CyQtWmY6VEvnZR7GRgSnZlVD2odO2X5QNjcYM49TR99vBp4y6rr77zlHSVKcZ7lqrdPAQQ
tq9m2SQM/56TffgKtt7Lnczkh5a8DrMXgjrf7wPXhra2Ei8i1oP+TBDlBrNY5O711d8Sy8XyUlDR
lczzQQ2WwlBfu12qvQ6jJE2HAdsILHYHsbUwoY6oraXjuDYidcPFTlMcIR1c9imlKnKlNsuFCyC6
6CK3cn687kaiiT9NE1/p08kiy55AGqNA7yy/RZrp3lNFmtFhWxyYsqs9GS7zWAZ1191X7Ms60Aus
r8ppAZ1u0qfmhlcs31p+F8Th9G7obaXIZK7+Yue9BsN6hx2DVBoErOoohW6S0gZGObpGH7YJA84P
BD+V21WkdSF4mi0658Ci5YUDMqhWZ78OjFk5vrJ97jFIX0GBcBBcqfnrLP3mq4DX1PfOoMlg/3ib
2Cm3IoBF5UX2ucwEchgDIn12N13a7ELoryAN3fxPWo8Z/CsAF06F1lR8ywwDQfF7nONc+Mtn/xoh
L7mblEnqT+DAcb3Iw8+e0rx7Ond+/zqQap4e2QI6P7NYMn3sSs3mIICxEwTosPbw55A5FStb2KMZ
yrYlk8tbzbO6jyaT1Asx1M2XzDC4tpR9HNoHQS41ndv11DruADLuuCKr54mVR80FUn6RSzALR+Lz
QsGJ5VeP28Qp5hH3x8QUP0iKTfO8BLV1cf4easIqHQ4ZRoKQWWFJwNk2yvKa06o6E0VMXJ5JGR0R
lezCwkB7B9VL2NbfMiRNrh1cVR0vL/4HNCvnR0pbA/i5uFUEx28oGiXxuc4t8Zf7AKJB8bcDIaC8
0F67is90PErhKg0dwA9qq6kkTd/OZaRfEmtqPxEgNyEw5nsjr+X6RAO7gK1JKWw1ks+eunLlpMu0
mNhzSO+RVe2bBDVBNj2bcdcQEQAOEnwOEZq8AggkWBfk6w1Ii7HUwMFFA2eTaqp37DkZmpH+cfaJ
SsO605DlOJEa3f5IpxD2KR2Fy6+NHZrgIOhMdq8kk+AJGSCzr3x1ypoeDBR53uSCpP6amw+lA8wu
WzNOMEdtZUToo7QgbtMHzHg7YD/5tOdYpKi2o4gZJ1bCI8Ms082w1lIsW4OpDPpquZHXBxIvFJyc
HotN+W2ThIesMP9z7hPTAFyo09nzXcg690Ql6r6E/SY1CowFwzOzdOiImZ9RJFlOXKb88gKopNx1
CZOWEhKYKqikyA4aGG5s0dKP5ETNHH8hLdB//+fGX6IsfsVqY3fEiaRhBZSOhEKU/emXBl2txzFi
tltCPCy6hA8BzMag0a43JRUBH4ZJy+ZYh9fdY9FFnbnTKjPSRXvfRemrtiv8/ZJkoy2Zyd6rPk13
UX6W0OhcQyw0+7do/3gMJCDHrGWSHexFutW4DEaYehtRb398haVnIywP4MxFLSxVbBXmeBZKRGk4
ilTeD057JI5QeDNuSuK0WPrLYMMoAxu/mZxAOOe1I+VXDjE+g9z24bXy5ubZld7/AzMgbFt2usaP
8ewkBkSR5WuWH4ic+nuZOMYu4kM1pczPw/OpB6zx3xl4GJv7HKuCZxHF+Zym0x7nflfbwnoP0o4A
KoyrgLYBYM79q4MHnAMnDW54WMppI04Nx81XxE/lQNIXTnukaifGRHyqWzP8/g3o+n93nfn1eufk
FaK1Mus0gjyTSkQnW0/FxcA0A7xoA5jvYpiXzgc3fgiFrt+mBs4PCrK9LSRCKxXci56p/f9VLhqD
Iv27/Ie7NnqiMSSMZC5KytuqrKhEsaT0N7MVzoI3SYBvcMzEAeAEqUnM1HZuRGFxwbsjvjpnTqaF
7hcgGiC/Utm3ez4EfOpvcWbNdwrmp00xyKPIxsjFuyMGv46qC66y5qhbx0YH3moUKHHjuflh13au
OWqjDHZSoVF5Q6XUCckYEmncwiLV1tW+Bdi9LLbagFFWABC0m+tDim+lSnC76sCWTL2tksCCDZLr
MhjT9BVdu7zIpoH8uEdza8T035mYkFX0e9wWee19RHiyWMjVxMO4W/wV4qcWNMOF3ThQFsuzx3a9
DLlvH2cw8ukyqFw6AwALBtwuY/OrG0WwbFe9yWCejv8dtousrOkuyopRzjl17qNhx377vr8gAlvV
E5CJhtpbfy5J2i025N89vRK4bFgtG9Bjz2c4fk5C2nibaWv5tu4OXDxQzfwXYm+NDDZL4lQ+iJR0
bixFwBTHcFJkqdcVbrlHsWprNRMOFEUtrlumdrexK8a18MFDcwS4hyewVpbNMivfZFtpBt4gVv4p
LkaQ2gHYJ7MpVQcKcqPwXztHwokJJQtQGUvsbZZrfYkCKwe1pQx/SOoi4xNTh7h3XquaZjV2lJo9
dmC6s7xrAIwsbfxZ8w4bYso6j6SvUWsdtxMBr4jugWzIxmlkZrKPIBXP8gKEvb3//oSlNm5Gt1Jd
YLfXlkUsrssOXIBHiXWbkzHNLVfJXI/9kC08GVAQVWbKbduyMgwEA9yG0gHsUCol+LKVhdANjLy5
i5rUezKCz7k+RvDH2XvMVfI0UrzKEa0uD6M64f/IWSjOyVK5H5IVt/R4FKJVk2nTmfljNP72GUYm
xL32C3p2Ry0nTBf0qSXOppm/tyLyaGKaljB9S5oEYWZEYlznyRJJQbTQobvKXb7Kk8rWylb+ccq1
8Sq7e1VNsRcc09+rRD4G0tbppuBzlPOsqV/vmKmpoLoGV1Bdtw11MfgIRuZImTyZ/Rg7yXIhSmMy
csDDoxSD9nrhEUZX43ORw7gxuWdJ8ID1QYkP8f05UT0pu0XfJ0K1tjZtRaBPhsDhhIVxhjwLI07s
Z/L20G2/GofHqrBk7b6YEnznnVu64MWxNatTRhjcLhJ440HQrXiek6MvzGznJkXOgoLeQag6gMW7
Pa03B3Wna8Uz0yAYU14weH+Gbvzapb+IhVd/FUDoIeM//jikodWi4j101HlFgEo8uy+TAoxbQdIz
Q9LKhG5gdyQ3iStYaD1YN87d0Pn2O8Q0iWQhqsA9jbUuovVEEXqfR8TpZasuex7dNMoMBBkBHDrD
l3Gq+7vDN83YcWhWPnOorfE5EJJ7TgFHCG5saQwHmlAsmJtKT4rfpT+J0ZnxZA8Xs7ri82nR9EIu
Nx7/FzN9j4A/PBbwa1G2eFrvORfNZAj8AOKGdg5AVwoXlaH7iEavp0HNRUfLDcZuJ8p5DOF8+LAJ
swuLveqGqVkkaqCs3zb1qsnYE/mMmEDKdT4peYxLL62khKOAjE6cTDboy+78lRPHTAvZpEbRKmJL
dC5puFR77HarsVD9l6Fhsg5PVBTs4ue48ZU905Ll08ogcBfUrOq20mac3Gry7uVAl6FMyqQKMA+x
RKKFi9Csy7Kaasw5egZCqFmAeD5X2aLQhivqblk2lWfby6gl8um1AaGufV8vwF7KB4f80M+hn4N4
GcI63MEy41VVZdZdyl0SiYjDxIsej7+HLAIOXCKbG2ED/etG0s18vM5Vs/8XThESu5z7eKOFNfLB
Dzx3o6fh50+lPkigXbuvrPkxCXGmdF2GY7fq5U+9eriE2lrdoi58LiuOMSBXbG6V5Gs4z4RDh9w3
PuuwopZHI6srFlwqzXqG9IMPGw9+pwTp0kJpow/OPli/HqGldhPlSpR/Mn8FYJF+BKBu6BR4TtaF
5rZONUZLiqsci+FjrgKdPemKc2gKpvM7G+mCiYY+gHdWBvnHIO5vg0KY6gmSXItEaS9aEmRg9ZXZ
6CQXMqT4W5jia24VFN9894V20LUT+8pf+kkfkL/BtMjCVavOYTqFot0iT4N12qkG2J/23PD9bk84
OWlUrvT1zdu0O0INgvGNqewN9FEXnLMGCsM+33rsgqfZz+06xYuYh4rzeCTvr7d+YLOFCqMh64zU
Qvc/5R9fq1HAglLD645Q2IxdZ9gXYrl0LFm9hD8f1qdIWA4UfMX93qyXKsw3RYUlQw83BneSe1vp
W8G4FhZkkWNE67Xal2BPwBI9yJAHtPtyybp1DhfxiMEmpJ6aNjGghYFPdJwfmps9uiHUWrsOnRgA
+Bzn0Z/AX888n2tfuWooeCKoci+izs2jCAUWdujRYdj04oxu7/RXkTW7prRs5QC75dMcKVcA6SeH
gHLU7uLzlFN8nfyRDs4XacrhzcntayCtKMkNEyJgRdR2jNrs/sVc8YdigKJvHivDN82C85l+iTE9
sGMg37wLifpUC4TW56Q8yYLeMyjLJ96rpzl+ZIUG6mf8laFDLowQbHbHvb/TIPTXtHd/X/jYniTm
Mcn+HXidxGywH/srFYRdpQWmr5/kTrCIOMb7ekBCVYYJ4avfefGcWDKtLwW+5plCjYKjYa/1q0tt
XKI5Jwp8hsUTuKP/6U4kj9eaoygjxdt8165HoE8QCvlvHZFXIvTHg9gldJCboJqVRIudW2lc9LOq
wh8f9oSORz3ggpA5hLMC2X06KRvWqPEpaIO+wv/VgNnFEPletqNBwTHxmNE3hO80Bz7MHjyb3Vko
HpxbQTEoJFaN0ELI7KBsE9lVx1G+q6lP6dmto7/kYFWMVPtEJT8D/ThOU5IcgpiPFv9+JRbf9DNm
pkT2jU+0jxvvnkSQgHei3If/FsgXRuNLL37gn51ShtDbWuyAsAfZQ70wQ2f8ihk4vOY5BlhNogaC
8Bp11bNhgJHBC2yStlc6+EmX2GgPf453CpbyY/rpbN+XISKtSXfw3SjtWwvUZJZ/aEho3FwtJtzo
BKTZTZ8/o8BqgB1vG2K59pyzr4IamsZkF4Nwiq5rT7SURONk+CKtQRGetKq5/2eFR42btSDutyir
fsOLL1EiIP03viFDrL3rlD38cXb1g2kQW6m+QZXEfpOET/1tj8s5PPGedgXnnTmwknaVKcUJ+PyT
cWVyeEVkmEb2WqHaJi6N4QIyPL8HFy7/g7gt1h2c7rieDgKpxezKQVptAMiJtUGf6jaW90Vv7x5p
4ralJ6ZH1KH3vmvjCFgn0PRSzWXcEhipbrlnjczf+y43pQNSuNjTWiFV3pNAo1K8KEB4u2ysa9Qp
p7RMRnUBVGdcns66/9MWMehF4RRfxVqbN832CGHMT9gMYjhnD1LvVXpkRFAlJkL8/6vqLlaP4eZZ
esny7mO1KlN5feWAQFIichajTFC91d3vXSrSxG+vG5IPFBdDoscrLQ9BX95QbSTXPyNcJhY7R7OH
W9kBzy5zEzxRvrGABiJSc+Gi+C9qtWddkA12ucF9thMD7uDNI8d/lvBVx5Zk0jptlgHqwxUsQZlf
pebwZZIB64zsirdIfAiIC9trl3y811vVspcwOqRPMSou+Qysl0oTl5rgEUUZ45eRCTntVBz3+lAw
1gZjVtyq5sc/mLFybUMA8tvoScqAKmp4c/ceiL8tYLo7rKh67X9ZSNk8HKFEiQKvQf5dTnh8HXza
nWs/z6tqfEAIN6RpvYUsoCDtVUCQYCQB5Vh3v0os/7B7W6TClOpoyQuM6UTrIrr+GexMECnrjH5G
4Ah7t1iS8N4+SeamQCqtT3TS1+mUUdE6um/mPLFXDtH+leVAr6VgGENUikjmE5T/5EqgvUT6B7xN
WAZVI3phKNdFKOIBDEgMtQzGZC79pwyaQJi6LIOnpMfo4ySMih0pzGLaVNDwqx+wHh4fw+EUxa0N
BQrcATDswKOkbLrL3mCSaAJl//Vy/VDQHkQ13XcrL4a/QCOGR1gnxff3Ec8KGrdc7oWyDITZE8UI
CJMJkwOErbREZMBa00WhmFZRxtflMRY1Qnn551jBcecdv4Z+MQhFQiquzwdn7DeJ7ICiRRIsGbjF
oINDw/zrmqdxMHAvch6LD3RCgNzTgMy1/Kpy/GFsXm415q9iMwF2hZ55cntl/Rs7Cs7aClO/UEYC
+VkxkR0q3WfkhV78dd5Ly5dDrt/rleRMFQHHjA1nUEIyX5X9jcBlCd/XIEKbRP0YZSh1QTiWtIjJ
HW1zvnYZzax2FH3gy7IAEkvzu7zZ+bshhJ/66dITqLTNqzIwinL/qSamOW9vog4HYN2zvSOQcJ4x
D1ulCZzaqz7xKCPpo4LmPdD4NQgJRaPN+IvQs6gLPmVSAzuDyXhAX5xH6nD/tSd7IGjTshpVvXiv
sc9QN8SxfOOaFhBp0kcrRjDshUy6Oj/tQDOTYKrifS0mxZpCUObnaect3i5GBPgXLLyvDB37eiWP
FCiU1J0+/N7Z2pQJefTCWNDs/EK2e87muQOBdkp7NgmjxWuZWZ1APDR88Cf2Zya8DLYG+KF0mcwV
BeCYxB+J6bbezo+phmt/huHS6PtVut7sssjoMAEgvcxFunpCBYmaRgH0dDmQ4yBrhViOmogtNIDC
vwsF7Rbe/OJElWZ+oRJ5jm7m7PLMg+PgnTil9AV/M9mLbdWzHYBijRw/YhT7c70tx5nNzL4J0oM1
TY5ulwpoA1sz800FePyjwRDmyWvQHVyZY0t3sL80CY9DocGWU2kkWjSF5eKooaxVneWodt2eYKql
cgRkbwDQyQdyFvomXUFhTUa6J5DS7XNEZxnE6BbT90ul5n9tzavmNs39KvghlH5INO5OfEDHfR0y
aslO1moNIITVbHz8Mx0kI2T7yxZ+Kds0p9fepe9cXKRDkVQEUtbaKklUY4fjFNlZJ2gvZq34ila6
pU+miJ7raSkjbtoE9Oi5eTzyqdGshqXmxldN/emVbXiT1P2iBV+K2ArB0KAhSCHdXCjwDjfqQuNb
QE2CPEfr5e5vPh8ZfEcd+MWDx+FPm9xZ1+tunloNa1JM8X0NTz4OIVDfzJzZALtC/HBA3s1F3qgX
EjANSKYqOGzuivuVNcR/L7xfPkEPAbD/qocW42xuxkImNYpxEuMGDX6VtTaccddRfDk4/jioXL2t
xUHtNl+2Rhlq8VkjteJN7bE+eCRLYxm7rtcMW+pA7NAH5j3jpeHJbLi27OKrBhsswBiQA5vfP2Hn
oQvaAf2NTwy99l2QY8ThDP4pnY0NBQuPiRRLTkpK34FKqgl8n/yxqaahv4Ue7hxzD5kuEmH9KtYp
X+27IK8eqEkUa3JSI7MjfU2CacajIYu5Q10SRYXwJJxZAsgfthx5ZJMfnnb1OGp5SSZWGqt9OcG7
RjJiFzfQzxuqyg9tqW7oov7aWGXTEniOrszcdLTfOTE6j8pH18NY+d+dBIw/T88Kne/T0kMu+zBl
TBFBt80nFz/yiFvJwtVaD+/V1xlxnES5e20l+fR2YQAhf+lAjcUirge3lTeT3jkoyequH867srxV
Wchv/T6D0Gv+DgROrmK0oe9BEreME7rtgvh8W7ARTmyezhG29QrjTFSl5qngpGkw3N4b58ROkE/z
fITyTHqg8Y8wdEpZmM+SvSx4gxOW/44pTo0IhqQKG0tytEB/1L7k9gsoFXNwfVeHid9Z5MxAbtHH
ylvt2x2wCw3xTWJFiuDGzIDKxrNQtpRml829J79x0l639gEdU02lIrz9a7D3XqVRJanBUp2uiH3v
ZC+q2+ZZ+Iph7iBwKAlgq2AAImTS0TgPRHovAKsMpUeuvjX83W2ai3JTcpe3uD1zeoy3mtQ1sqSH
cjMWCyn08XfFvsYyRj4LGwQhW+Z5xCVoqoI0khg71YJaLUFTHrV0S3Wsmfc6d3tMu35BljZyicly
BdzmOp73oLj4+Zv5g/lvqIR5cMRTzYIMcBIFc4KvP6U5d04EzO4Zp7vTqi6+RJpmpgHmqto6/4OS
qbSs1XWzfRgspcdT6NKz0VCT2uz9eb6Fhj0MZP77+fFNh5a50V6tMQNQRR6rxrpQTPQ0tYhsK7j2
S1khLsV9GMmpGMOfvuf0GFZkMmEcGYPw4ylh6BRFKJLVubdMACnTI8LlUlv8GRcaXoA/9Xt3wfAf
ZyWhG8cZJuYIx1NVhcACjxQqevhpbqsNaWOBz2GkyxUWgBxwrp5ZIfgvSRvdiPwFaS0yW/r8bi1O
B1pXI0/3XEBllL4Wdqb062aNfEtM0vSjNVqiKSPwME34YCXNxfENDA5GIL/a8gCeVDU+OET5lLSk
HV7I/KGWA2pfz2aYH9WFaSbHn3Z88GoGqVsO8ZQRuUcHyJu3RptzCoIfoLwmUPXc71l1sczK5JTS
LR3Ta5K9itrWNlNAxb9JYmxBuet5Zbx2ovMs4PGKZQu1hLeTrr8vQAAn+8SuokIPtEwKbue5rZ9g
rFjyD0YQQYtRocAwLwPKe+Bl5MCpm9IV5HHXfu38ovXHRFb/J8o7KVYrErPI9g3YzxljfbrDBd/B
2vWHyC+evGKt5mI439fCSDPxzMw1t2YxtjijXRoQUD1wwt8h4GXoikYcvzQi5C1ORMGq1rdPJyXn
xM5i780PF86c2cFsRLOtcZJjqz9EzovSd1lEGkB2fFKt9g7kxVo23E5yKbHQOfZXrKl4mlzh6X48
09Tm+l16BKnJyGyxsjvS4caVwZgse4VrJW043zrS0vsMciuZBGmURycVzhQ2vL3Bm/BxO4zgIJFl
AzE0Ogj1BgnLgBm3/Kb32j2E8hDef/5OTSk1QxuFwf5MLDnShxXc9Xan/yyppgQLb3xR4Ql7UAw7
zSh2RZt5CaUT0KPjBShN1kZms2VviamIkJujYYj3l0//5POL2fSaa/n36hIq6GhUZlW2VSfcZuJ1
yL1nRxgl1so/MbsEjgr9aCm7qi6mg6Ma2DXH1qeW7nckAuLKAiN5F42l5p2kxGw+ts9jTBbgVlt9
vbfZS+xylS05XlnFXqwHqP6lSK6FeY9UUbV7IjagNM4TJwRYLtCp1ADcWkatS94DJ9CAmruAngjd
zIVIoGukqqXZ97Ek33/Cp93+9JQqL3QCX4kLQXPLMjlVo+IczJZ9QR8vcAgmjOWmWhQSPxevdFVr
+27RMbjNAkYN0sRFgrJ6jmxx/MnK7z0pPyzgaLL6UwBq/7SH1lfSrYcZ/TJWKlFR4CxuFQr68b3N
mWlCwiPBTSylYm0coFA/4UYCAk+SOf9l/TKOuPLzRRz+xGHmVEf+pVYNHmtaUpwjvedWo8KOeNs2
CnxNVAIprouRIGUqj/VDvS9ewKFXCM6OGEJIpj1X1v0EPTT9uSctpYbrX+8c2ar+BdHT8qcvSDa8
2WQssuDAXKgpX8sQW0aQtnRqNyhVPEOCv1P5g3mynfXn78n6IUScqeJ8kNp7Nnzi24AUS0XaoE7l
y5R0PubtQKlvj5pcAzVVgEleuA20Wi0M+CXqSp/N6jzQCZqNyTpUPLWGVGXhFTLC6fJqArEHrD2z
pohS291+4u6+gC3jaldYrP6+kUPyZUXqs3gDyARS/8ZVvmo4dli1omomer5GJqHxwJSiWgloyII+
QukuvXP8s5VSEsEVdxgA6BEmsWQvip7TNlHJd5VTzlfi5/pnkaU7ziSbvmVU2VAiBHcW2ZKein9e
4dyFsAN0BgjRztQ+Mwj2omVO2S0TS3y4T9lGEwBTUdQvePUDKE2RitIBcQH9MonJYMe1W9iPMyot
25z21Bjt/bUUzQ+8s65f9ynMB+RCyAaaXM+mcUBNGE3bEizY9KW+qS7FUKjF8KM9mNsMCouNMbJO
/A6YMsldN4loCYPQeKVDL0qwDX4+UskrnJM6ZK9A9GrfopT6vW3huiwKL5uDHXvrhD64vfbagP1o
6lstECIwQWigVTMQkjMULH6LxCZsU25CF9xubRtzoyHEm3+N1qD/F6EBK2pWpHFUHZtt8S+16C6R
6c7ZdesfTFo9rVRc+7LMciP2LqYzb60ntZ1rfWE85zcphJ2almvVmiNSwujh/tRsiHTbprcfGf3a
TDRb931PLvSqtDOlE4jtkUubaJUvbFszLuDWdjzkHasO5KO6Z0ZtbVFEeJ1jgtL3ic5zvH4Htkf0
fD24M0B8MgLYuBZF/zly5/fHYUGT1q4HmOw42XJVtyg9PLd+TXFZ/1JCEXKUBcNkBl1q9RWIZFcG
+KBTfZ6JRFwNDNmRVPC5B6wfzHTBbk9aojTuPZi5/XxxCcMave0VnmLtVTi6/407VP4mnH4LXrl8
vv7kq39RgAmzbPuf+Bk8HphqxBFr3cxkRYp4/7ChZ8fWC8cjOEiQQK+UfKiKM7ZXwyAtZ/Ljx/Ax
94mdQ92HnDP1gFiGI7XCpDQgPhzjS4mgl/LIBzvA+sWNAmJhrfW7VAlYZypoX57eHhVvcUyWGlsz
CcucgIQoYmNZ7S9UG2hfldE/aFE+hiQxZVlWjwsSOTC1faDLDH09OPOH7PirqnT0qo9RicBbc6Z5
wIH4qy7KyvmDqaLkqLwTCBLaRlw4OivhSwX0bt7BYUPnP9H1rUJckFzCZ87MXu80iak3iQvh6Msq
VO0WDNSrBkcH9ep+WOPBN4WQMCpwIdrsyW7swUvDdPUtwb+bC5t6tHg0n23QjwfV+CBauxvBrg1U
RSyNUm1vF9NVaPdzld5ALSlmT9GHR4ho5uwhTO+EO7xnHp6XBgJbmN+W1sGy5oaNidGnNY9MtBZB
+zDCahKESQ3N8d/SZl548UuPV05JeF457qT7rjnJDM8Yh0k715KswhcIvL72Jt/KZ49F0/R0hreA
PbhIB1ed+9dBaf+I42T8Eiz0b8SB5MIiERyAryu6OXu9yy/1iC+3UjnQaUJXtBYFk+xbfqXFguj5
AhyFu3SuhgylW0NrxXqK5KhfUZBA2T/KbcA2RB/e3i1muaODTlQMfKV/eGLIMcE0nvfIcKGtnjKD
rOPt7HX+/u9O1QcjXgeDBprpWrjZL/Rs64RgX3MfX3wzYDogj6S7B8Aah4FNOuK4bKRma/VpPaiw
xjEmV0LegfxiabmlRPQRvbd6aa97xsp1IPs3itWH5LMV3Jj8TPGc5czepo2hS4FLSwfjDKEeWOUm
AyuYuWHjCio7RSfD4vwKOWb+36eKR7+3wXT8ONQEpe1r6uTessUfX+Fk+KozRlaHG61A9AksOiwr
/xkD1eKE4kU7A6HM64AMJ5G+Ag16G5dQquD4q+McIwLmyzEoFbigWb/aT7MHUORQ3+lCbX5UI4Ms
1s7Ewr2+WnIpsX6wMiF2//stR4Lif4qNl5J88VLBxf9HfQj+hdf1eGRn3K7mVaZakEjTRyII/DUu
c4MeoyQzDe+OCFiNZypwYpnDH0/w2hLz4A2DpZsD0LdoHsOAQN6YsxF2YE+3p98rx1pxVwXZQUOv
0Vt52Psdn327oJycwVpRrgLKlCnktHr39bFCbJR55x6qQHKytuwgD20ql4OBEs1tMO7g2lyqG/lN
iisE1/pfeaecX5cpEaAzrvzDAhcOzOkFqQP2gTdIIlmlBdUMlmBelxcPkcv2h/Es53ndcUyB4V39
fOL4eJEs5iDzhQXEHF4mSjyCXY6erkZ/HSZr5TpffrAAAX00n0hPNVUUs8GTJQA0hCXrxJ2MrXLu
aK92oI+WkVXrZwlZRLrex9j2Nmj026jhinvGpBPaQlBmbanFDqncyyonZjUQro05OX270dKqAXbQ
lk8u7iCQyiC/I8ckwE6Hk8uQn2O52U1YDbsFOhk3QgqLBZJp1+3y6F3LCpHaA5/eZDawMQsDEY70
dK2Z16zIKWEGk/zNoPdVGe+AWDkuXI1ICTClq4uFakG6x801mnrnhRlBhaUxrtsC9YXipKH3ZV5O
3uiBy9+lKHG5Jk11uTe2XvDAnXhDp6A/YpEqW0+bVxVq/wt2abE6Hec76px8rXHmXD6Kh2HZnkeC
7ZYVjyZfCgSJxXkANd0yNPKbaqVcp+MqIWyfNA6VrzTCGnTzezLIZ1zwE1RqP5tSb202D5VVkYOm
vQN2vshUvQ5nVNrUFQ9Y/O3mxwF/cwZ9ywybFdkN7DrJRKdKj0sMEeGlYGRfGHSSE/ODu240WI8E
upVk9IXo3giw9tZcWCFfLumlwgZg/qiv9AQsbOtpgr/xZ8DdvogI5mvSnfIndB3q7rIxy5lQNXxs
3OIoRqcH7j8ufwkc7nwYRG/dskNgk2lYnHf79ngSAyw8Cydg9nmMvDlvQyd1VegxoD2ipRu5zZfF
mw3dZjvDXbn2JecMuFIpVp5ePJll6lQ9HXakUhPm++O3iLvSpbWy9sJbT5Jkva6D82FLeTs5Ds2f
nyM0K8iAVZRUHQXhtr5ajvY2R2sjba8jQUhKQN1O9Eeq/9wMIjk2V7FzE3vcFCpiS/ATX8LZe77t
Nya4SHhoz/lPW1GxsqmfKwOpJYYogP9PZKSl4HEHjWQZEVXOlklM7XyHCs6/aNfVkOWsTQu/peH6
OAi1ri3Un7LVtPmoGekVdzRoYLul0nsfs+7rOc4xascfaqjrmCwQ0QWWJgWdd6iaBLaixTYdPuGC
524ZQILn4km5quXSLguAZ2SDP3okdD90kTg5QhuvdDCFIak+hVSV3JBOh8HvioJDf9NkuiRmYHWk
aQfYGwMTEq3p1yWWTuHUhbZQSn4y5fQNZcgdSQRkY1qyiAhShKSExJUhiVGhE96JcXKs5GpwnL0e
2O2gnSGFUxdlTryPMDBwuxxgCcfVAYSNLeAOrvgcc6Mg9obeTGGpzDEgqa3iZu5vAxDcTbkKLX7l
zmZ43gSopK/kayGRz21smw+FbQazmqOVQuerTDzyxaSIZOMAzBV+82EizTMBMMeGJOTvaF6pkvP6
0Tyl1wm3skYLezG++GgruQp5SNNXG4eaDnAjh7NqlgsEhAEwBFYSbtxc1QVioxk1nhf4qDi9OD88
MimoaT+YqTdefvN/0cKKe0Z9wcv/Z1YOIFLshPXiyj3bcerVeKQAi7kIGeHW2FtWlwQnSkqeWgKv
ZMmBX0oEjjDCXjHvecfH13DRGgS2OU2imhphpfAlc0RAvw/0GfasZAxQZYNCGtOFKZy1T87leYZT
bQZ2xQ4X1iPrWWCmwgiKjvAgxY1rBbzqUkVH6jdwksueCsSia+vf1GoqqRl/MSEKo7ftmGrzsR3r
7l+DgKN322246gKzoFOWXgpibf00NlwGOY5n9hP5eN4W34T9gDg9mj4vlHjEE1wEacbfjqDR/bYY
FG8FDOx0F5qjV7+NzMUdcXkEIkP4ZOU7DWi8/6OF57V3lDP9XsgfqMcr+eFfpXPzR2dyXfaEDElw
UBU7K8VJBKHFtBayOsVm+Z9UWquG+w23FVuI8B/WW6OcIqbdcckFhN9On15Logo0qqb3iD+fSotZ
W6Jy3YARS2sUqFMxzCQfgDdDO7MJSc32rL5zihI4+7snqxpePUSp3EcdCSVBHFkvam/6KVsVNAXc
8bNgP84P8xx/8lDQ1gZqR3JyGsiw6HQJwFqmsLetWzInuttOpQuChq6LwdW/KW8IoKEJWw5IF8vQ
58RQJTdqkNvi21qSLFRvysnWfGi53p0OMr2ebFOAOT/YTxn/ZBKNcUxGYtc8K/5N6n3kUY+Yt0V+
Bs1OmM1A/KdnBCxMDiLb3YOaBN4HBrLR1325Yswcc4QGptgBXITBzeVQK2EWmeCFTwVGXYp0jFvE
XBJ1eS/cOGoysk12CDA1MauHhqoUe988ykQeQKiU12Aaeqnj9/wC7Igk5tfqKCe6fGMsFVUnp09s
3r+/wM3PMJIyC+CbQ31y/E5eKVrFtqtlhi1BqEQkaKlxyx+knBx94MgtsErjdJlAU/BN9nGYtt7T
wmLeETrUL46fkHn4P3Jv2pp/5wt96XfMA0xXIT5WBKiHuQkksNHt99N8arNAMNuZe+4IPRtth8sd
CP26WQYpXgRVf/5RprbjYpnxcutJZRRpToYIbcLk0/HC0y3v4pTItaeh6Ujewkl9EjtyIxpmby7u
1ik4FKv70j4K+uyErw+8KFJQF08nZjrzvyKMBDHXOpTQYsiuqqUWWgkGKpSOS8pqSqbNiJ/QsUZ6
7z7Ofv12x5Dvx9HmhjlPn5TeRtMqbl3a2gsy5swqGnFKfYhOgCBiu2tRFl7k+3I7jJ511edwswxr
DXg3ebezZzt+0she/qyO3eHMaOTEKw+x81depEz2IXSOm/vz2m9xNt2aO2BB4x9bWbLrMpahYEh7
dEfkSBNLlMxRXAISZ2Qf+GSi5DcaHFDdwCLXm9G6OlY4OnFwyzXo3mLJuMdquM/PD8hI2Vr5Jxm9
QeJBY/Trn9zvHbMjCVmqS40KDWN8puojk5k/pQ8bMFxbHeshVsoKWKV8GeOghfA2TUAVPeFSYZA4
IT53sREk0eFDAtE5+jhCIcv1ic3w5g8GeCTBVKcSWR+LdYJlqPwhNXDJshaviT82r5doqc8azibv
5SXczS1gbH413GzarECtdY3YsAdvdrR6SBWix73u9K974tzQjvt7nfT8lOPU5LpXbsLmBsqMnfFF
PHUowiIgGB1vJFpYFI6eWuv/XF7dIvb5VLrJgbYekVlWTHaumC8xPlBpf4NOseZMPJmujb/5Ehl0
mY/8lxP8gizLZFKGtppztGGqEdSkb6ZoOs2dstiJR4XmZPCWh0fjmV7CC55PWTQE/TrwbC6oVZgP
aQx2Xq0vKXJfIErn+VOy6MiCCWcPjmB7GbMDjzOJAr9uazMbQtIgOgepVnG5puwYcVirqdYTwGHT
/5Z1ONfkRvT8aqbUgYPyAhy3opF1NoXKAciR5aecv1xPdpkmOeM9IyNIHyibUbE6W70O87lznHt8
7CqbFy7jSe5Dk/1ruJu0/LEz/wuMFNKLMqy8EwJXuhVgRC+T9nYpXPzMdtnqAOXhLYVnrYUbnsL1
Cpqepma4Gro/jNvAS5VB1ZdfBwM1L1l5YL7S+fFXkglpULg/hyGBVbyUrH1l0JuEYuyxo5ylBcCk
LxxWiab+AYp8HYiUMn8JGYoS4Ww1613xlDB1Yuo3gCZP8rPaAs1nKNuIKNX56Sn9ursRyGrgousM
U3ENOZ9t1MtyxNL2HQdbU12sExwygM1HmAkA7Bf6a9uPb2ZJ6zV4Io4fEdH9pil9EncN51YdjG+q
TiD1BK3w9BhvbYxFoAxub7vnaM0a9sHlo4VAV/6aoo0sVYxE5DDX7TxKta/83va3bnqny4LgoeRl
GpHMO2qNVCli2dmDRrQC17dImq/HwJRY8SC5/Htc57dA1kXruO82GzYIVODj/v4xYn0Rt1jDraKJ
AFbUt0rnuNB2ft+FYibrY6sby42p+sTx2VUWwCqayYuSuMP3lPkr4P5wTH3GwC0CMyY+LgZwNDam
ac7IK6RCvp/5xg5fDJjwZPlmdNKpM5n91fHGgLVcAn9cChymjy+eaz5s1c8z2uHXv5Wmv9yEq2gu
vqloSfbNI1AAPBtaWNOLHUzx5X9uI/+k6GG6ppUq9FTkQE0Jo47lGmeI9FO8IRaSGtv3zF1mlU4m
UjeR3hNv/Map5bjmce4gIoIUx5PTfmc7v3Lvh2hjL++zJASHeb9xJFe1XUwOawlCfiGF4kcD/2pd
dSG6XaWn5UeiE2ETysWta0pwmQwIKWCaJoPexwiLmsLhqSDFCDq8KEUcUMRDoGZfa/N5HU5qoIH0
V+yaPg5ja/y+phT5AX4AO+D1fHvMFj6AUgB4m98p6oRJQjUo3gRRVnU+tII9bRfuw3QsWlEaMErs
q+GgBsnX/oa+LVcmzemUIXD+MlZxmVGHxSbsyUzoOio9cgI+BCFyxU2zzFqNyQAKaMy08Oucxo7C
2KEvbkd9x6rlxswA5gYDhss0YKiOB536h/oo1R59jpjz3W1u4twMBA/QbdO3HJXYMddj/OkbOyRj
r4T2E+0eg06ljgdu8ysy2/ZBTR8IugqxIjAwP2DpycrUnjdtB3VzGLGonO6Dl3gc21oBA2F3dlPX
q2gGZaWhpsyUfVsmGoX5QIYb71wFsMfPgHUv5D+oHAiXtJgJQ/qimg4U5nQ3SopwQs0B36vO2Fyo
M5aeEg4IK+V0sS3Jqwv6O86MIllAP9uucwD+6ONutglPvNoNFM8VfKPAtQXMxeeaWQ8xpmFcH58e
HtK9CI9qkYR1EnolqQeVqEWpEe21ozD5DbsxcyX7bZOWuoM5sSLTamJOMPn+/lNP2zBoFTBUYzrG
+9nHo1/lgvca+ESBwnwWhF5a3VZ6735BWNjqgCfLs62Ube4QLc2skr8R1AnYSzaNdzxSZloAoDP8
39OoU1clXc3kyGBjzFPoM+UjuOFT/gOzICYAFG4psZD6JDLEggjj27xRf3jFrWZCvKB4nL3jd4OK
UrsEeq49rgypo7gKuxMY9ZqtqgDVpi5Q/YL8JhiDZCgiiCsIJBReGk95Yn8+DRzmWOyzUVtLSci5
jCwsa/3yhW/s54fsYO2s67mgHBLvAVqPnMqZeVOu1nmxJmRedO2V2aGj+z+WYslhe52EN4nPNFw/
kNEuN3xfGNjJAn3lLrTYiO76ULXXm5lB48HhGXQtGBBrP138Avikx7aKt58E5tolM3FfUXYmQDcJ
w5mu3FBM1DOZkVKNhACwHRbH46f/cF+LnpwJmW4IkVl1UZfty663UkKkON9lQYP5fxz+zp7jPGzx
CV4TrP5C7OgEHEz4PtyECc41qEOK3ZQ6HFuFnGOYBzGofj62VfwZDryswP3PPPEeM1KohPrt/XLV
GOwGW/1zxO4d3rYSvtj18WAwFoZme3I2wkaNLntl7MusQah6mLR1HM79ZjBCLZ8hMUi5tg5AEg0H
/WsCaFX9EFNhslTENlwJATFjHthfyAA/1v3Kx8BRDWeOmt/GEUmbmKb0GsNiqAmfYyaLbYqc0zfG
UmRR9SOoHMNKALUhqDd90DU1yzf5y2VXVwoA7I9vZAFJsleqnh/lVIt1lLynw3CzqAIRGcTm5rPu
9uyCgy8qJOqh2989n7o2tpyXHNRaRsqUcZ4bNbSSNmlplJrNlitydpCDt/ueguAiZg6pWkcAnuOa
tCOtwgoV+LsiaTcYQCcc0LKygDUYWRQjBJKcyLsv+cNW+Rm22Di1y5GxrGCJ9BnBTPHM5wRKLAw/
02MNCzIOdyhC/btWJUA0HjPWIjIQciEwCY5u94g3lvS3wCtWRXsyZgQGAe5BVSSUcvN+YLRBNKf7
k1inCPchWUBsSoNStgml/s61WvuBS96Mk9GRCRZrNgzEETxsJDwMM9kFH9QiPcVxMQWxFdDyeQXr
j+wNTtN+gMYgEiQSt4IY/C12ofnTCuUNXRJt9/eW7X1mDpX8HSqRKo+XrnJWNbVRHeCURODgPoSv
riLMR5SNSlXHG+Msm+TKxaz3tcGTS/yIYWmViiLUYYa/UnahzX7K3+EG2bj+63OcXwJRtJ7C7I4D
+Rt5WgJTyqmLWbtaQkc8gw9Phj5hBL+ePu00IT/alQA7sGrpwfI1CessRDw9e+4NvC03XhTuZCt3
SlMjeHe0Q67co0abSCAJ2e98WTlnZMDa6njCR0DhrpwyWIwR7BDAUVbGNc5X/qweTVrzpZyBWVIW
Qe1vRNFhpuLr6OuQ+MquuMndhvx/B5uH1nciLbdhkf1MC13L3r1VHiq5kbSd9SMJsP+bgPH7mKIk
UmjFdlv5Nqd1DiA97xHALnUb2YGjTs+EiNVHQKXZUYx3QPnaGcVPiXUL1gM8E6yZ6XNu0LflTszc
JsxM33vDdeCXZc0tgJBIzGXdlrbuba8ZYOWDKr5OdYCNhfHzAG4h7Lwsjodvq5ijhT7ZesTRM6ik
muK49sGqi9ChCvLH8H+YTbfv0543cZPp/GJ40sZOSeoN4XlCgrOLma94ztj7caCK9lhnoga6zeQA
Qjz1VSFhAhg+xLoQy3qrOagCah6WQ/t+VK3XVbhAqYKY6I2LHOw4oGIxIZIxLWg/4/CbCpHGPfcX
B44o4tHIlMIBDWja861vziOY46RSeG1I+yLIb9djECCRkryaXnuY1B2ofQly16mwybzG+ZAw4wdo
1unT+9HMFp+Ovafp8iaMsHT11yT8VHEJi7ju+owHrn3UrYNIxKr/Jg+W9SjSN6J/kSfgAx/Pppx8
cUyB26lJbq3QPHJE6z5AJfYUa6WXdAWxtiLIzqHf9CEMfDrzkuIIzqp00C9VIREJCabBHmbn2/wW
he7c2N8c2BqzSP3I/ZMKWBWQGflyC9g7nd8Ss2o9FEoYxhiADzzO1mrWjgl0C+IBzTfc7kcwziA7
Qx3xhAvDgi3j88LxpXnxKhpOoU2Vbs8YEBDGu+DiSCmKrt8HBUQaV+eHsQ/K5D7dCIULiVsYQv5y
aTl43QGeOaEEFVnLADaJautbnzFmt9DWdsjGdtYKcPcoee09OMldRZKl+YekGRBiqAT97k2INu2/
+mHv6rl/F2QZqPMdlcBbmo/5cYbh3jl/U6Wvxjb4F/kXVBXl3aQELVTW2zlbXM5iM12aRaXmU10P
jmcxuUD9j+T4kj/1OD/lBPUR8GhTdImhYw2T3MeR5V4yq7ARtJ9VwFGv2/49vv7rqsicKrDH/gcS
QfEiM1EhzygvaPto+TfV5i4oGtGq5lZayV3lzbC12m7twVLrY6+QZ7m+/vwjfLahzjOycO2gwN2A
LKdXzIK4EQHKL0y1xKPZ6eVxJDvivfL7CpbPIgjlMwFvYFqdM5+EGvUuEJ3pLIFDj6Skc3j0O8ZZ
Q0H0IU2NIpTomntBlG0hjxX3ZTkgaq5Pr22WJzWfifG9DGtVDGEcSJEB2/YA8xLRyX7p3ActPjk7
IWZMbKt709E+aRjOeiZmUc3ghIfbSEgI/bP8J11DL6xq+c7L2/HsGCUBojeDlh+1djsNhNl6y+1n
JLMdAAlCpOiRJRrWQCr/KPsDZgYBkVNzmMofqIRe8i2T3f8XDFWlwbEKT7TkCX1beLO4t3akpihF
51wKFwsIGASTJeTUUckTHLi/+gTozPFrT6Lz/0WNimZxjeWK+fp51y73JOG2t15i2Sf2Kqb9gwjb
+jQIQWnEdJRpOKGDA497A0JsLOCLSByCJA4JErAxZjUX4sLcTLWZLPlqG0DwQ7vqEWyxBdlgEVl7
96tb2LTIA5p12alg7pbSwOA3ihIhOHPzr+1NvDUYo3f9GxRsrtx87NvsYUaMiRkueYxeFI6RwMa3
T+pDeSciC5s2r4Pi20wJJ/O82LibzHBfr26hzFn04+4v5oEORaE6UwSOJtpg04r28dGLC8jAuB48
oyDwLc3FbSpxA4a9etHaunFvjS6fUV9dYFDo0B5pNgEiI7MMGBzBKAdKP8r1nuPBy5WmFBTmeXdY
+Lp6j4e+6YRN40z6iSiR0c89Jw2B0UD4xTABt0ZHrJ1b8tqkPfNCmBKGuJa3eOMoewrwY7ywzO8O
R4qQp32RqEirMZPOV2Ysy1C7iJykWzbHe7EVUg7rZqFHeFOiey4GjUqQ5C36IshWa21KMpPuv/J+
0YsyHJb+KoxhLo3aBJltA9/BX6iEPRUXq9xh7PY2LZe9MetpPNDbuvbg7gymmfA9iucqWG+SACbj
SXP6qew0cRCc2cCJ9MV4pivE+U2tWVSlAfojY/ECqBZvcUF8hcljIrKwZaAwlgP/6s9V1BhpJZvd
sQvR9W+ucO8V+dGdCCnsEmiVYYIQaokZVBtVkfCr9aXvffxmYiIaOuqZIvqacGQLs9AaedraCRc2
eSAy+1GPwYoZkM5sNfkncpeqPgQdnVVQZ1sDACoXSvJy0jxiVRz5+s0CNIv2YYxIUbVn1YRP33zJ
MwyTQfhi7pjRfKX+PhgoGurEpN3i7UratbWa0mhDX3HQQAXZmutKqURWgPUJfksesgVJR00mHGwh
uDKZgwQUDVj/2iFYeR+8fSqfyo+obRwyPgmCjarlVmzSvulppbPi1RAV9TcoIFuK6tr+r4YjktFB
IwJcgP5E8osujgJ7267fOrvBiQ/hXcCGWV8Tjq/2URovyMAQlawlhQykH36tZQNdJb7Df3aSBk8B
lTaCyCf03TlcmscXEe2NC9LZbPOxK2lOIixU1h+p4/VkA6BZmaFJY+YSKAOSTyf0eyjsOaLeqDQj
yhL3t0pq9Qe99u131Snymq+VEL/HyCCLrVYQaPyLKwky4KYcx6Ij0LCYhYsIi9k+fkZY/6lJuOHY
U6CnA46d1K6BPz0+ttXUk8Qe+/9V1sTJb0UVzomvCH4Sz+nN2NlsU+SxVhMJVBFlFgO0RmIh+ETx
yAHbQiS3mvNy8uG5/Y2yvxnaGXZWa0oKsc5ZrCJxsuGNlU4PY3vOiQhOwMlNhZ7pl7A4muI0//uP
rg20TqaUjri3C3duzo5kUISMUNwUPCVwYt8YS+ygRxbjkad4Jc/B3teBV1NvbvGiOceS4xWdNlJM
2J90nw+Oyp7ZzQfudL/UsbI4OinM+wM9V4vTxU+vRU9InFv9c9amZzQlsaB4yReOVKddWHSrdBwt
cUpFnvTb8vKZ2D144vJsbtKzD5eZFymgFdLsPQWFlumXluMoaibptOXJCEbw3MtCLtgqLvNkws7F
CH8wLSn4ka8bI0hsbC9KfDsblBzkwrItxvxu8+kRKde+lt7KHQr/mLc+B3ow766v6Pl5YkExCRkr
oufibQxhSMxZ6Mq/kIFZWWdU1162/5upwoHmZ4RHi2tmlgmTgVCHSu4rp4LpLt9kpuZQTclxmOSx
DpAUNTqQ7NRZv2NvYmzmllF/TXLH3YQlHCCsJKp952vXozeWGnV7kZ/Eq0MGbA9CWjh3XZ+ybZNV
K43xExLDZaQjfLnlllRYXbLE0K7LcvUNUgSht/wULUcSZh3rMy79hP+ohZtwsbmrQGtGVbJzQTRR
hhbODDffPMGfHt6FbQcSIkk/7GptT1zXWIPXUE7hc0+EpOVHYRQ6GzffKvQgLRwwFRQKs1NOoIS5
pRq7r7a4Hp/VAYv+V/+5Dqk08Un9QTeHNozkydnQBeKbeUnLxp3G8ysFi2LZHQ0xCDFjg/CFsXdD
vGI3z1kh25ts9q64EOOTlQwycpeaRwxVag0u8AtZk+sQrxXdLW8sR1KwztALm4cKp5AIqacXKBd+
6rsNZ6U+FaPMvKzJgozxk1WQZN6knU6jB0wBxyOWUtqdYo4VFDy0PIphliMWy2H9sWwd9C38LvSi
d8nOhy34PYfXU0XGU2TS9RiBn8ta2pZVunzG+WZXms8yLAC7IZXXlf/lqcrhWcVoTQYIcbXAKHKl
waAL339WRhzek1HxbKjdqoYh+UZru3Iw3sBAXrp5HsUcmtdvWYCp9FuSGKaIgpSbPd6/yJKn/HW/
s5fBoEtmRXAFk+0Q+E4BbnRvSog3wuDs4X7iFD8ZXMmv47KCA3ZDGwoulnF/+a2UwUduAQmhJaZ7
xg/VEXlh10V15vaKlhjeqT3d+JXjftH51nHftyDvZXNw3D1mMKrT1aizj9srcdFZ5y8TpjMwbOts
LEYRqXsRfep0bE8MwSwORqpZnXhC5ejtfHagqaWe5HEZ0kZ/nBNosSMlShm+aj3eqL4Zzr3zrc1l
LPioTN71GMPxdvsHVwc66+nk1zJNenT4GXHihl/cnOOvrH+YPtp/PXwldkpDCNzsLDg3jS0uQ+E3
TVOuTUeqcHG06FFK7XUt3AjWuFuqnjiC+Cm/L8madPoIuBr4qBVPUPxDMnSmQKE2Bou5RkMOlrOf
C19OJPjmb7tQY5Yl3agVO0EfpDvbTku7xJg/0yGCYVm2io6sO6p9fJY8dppcOElVo+KigFLN1dcr
8A1QnfPfDW9FcRwsaVkWk8sQSQBjeLO7JuD+rBwxzj6G7joUTjj09+sOvJkpA5B1UYdSTB+V459x
N1KH85dFshhUU8kV9maK3NhQuYInnoPthoO3wxaSynVGW3NQyyJS6J058Ofjsp5hUfATfs5RolCF
WdnZiq4FL84p1AvU/IdcGh8LBWnVK5h24i2Dujd2jVVdahBT4Yw6VjxLC2PpAUeUm77YzcFu5vpn
knK08Rrs5ZX9qbThcV1LOBgtn1onq+ggDH2v7XLlhJ+H8ZzxunSlP8lthMgA7IGHiHIwGNe/x542
F4lG+wpz+8+phQgG9JzJ8I8/XSywYffZWzfFU1QBeoN9Lz3d9aa7zV8NsQJ3ZaEw/EsWXbfkGA7p
pE40d+L6ThsJth+8uDabWZOgLb00+0J27MMXzjpgvgRjFLA6/Fu2NEXJUMLA+qxIoxR6UrW0QY8v
upKBmOFMCOzwMG6/v9A+hEtyFshIyFPJeGZyUEZ99opnif1NODkmQZSOtsLiUjq0BliIRHTalYJc
R5TVJsB5CEkEN47Dn/r62OtVq7On9hccm/KUQJ/F9r3MQXKahF+fg0+R0SxX9G9Wltotg2qegBCU
9pCNiTcnfmz1Pm/yIY9YnAQWEs2flfWCwmIs1EDOq5MeY5mfnq/KSKZ1UlBs84RZHf1lFd8vCKxM
Ag5fMUqVs46HQ/Nam4XgUJgbewziJdJoUUvDc69o+QqAE9BgbgIiT2SL+grellbmSoyKELMNG2Iw
005rc1rQWWFDOioTmxpBOfmQGaeo7SjuoiEM6oToYA7hvcxEw8qqgEIdi69rZDKeewabAke7tTZL
UtPVVpcYfFyWs79pngNiozHMhMZbn+XP/AfLh//VKnP6a1clXo+Ycy244lhMucpBV8b+ac+QWEAw
Tf+K3nrUGLdQRGu+oiCjNtRN/4TPk0jPmtGfDvlt/xbj42nw2xZJpVd+S+KHbD/kaZKksUZ89mwS
0/H2ASDTpOZ5lp/dMq+uVXTbEvq5MCGsLJ7i/YFWskZGkrjKfH3vuzq2lm9JXOVoi4hhz1KcakLG
bWXR7gDboT0w2B8s/5zOe1BadmrvwEkbYxZpPpXRVOlEKLkWr2Vh0l0jm3BDC7thh3KftKf2WYfL
Jfcc+2rSUNY85Hn7a0q+pwnB1oc05VLYS2aUsriEO/vV7nqsfeXHZS1j7Jo+GR1ebbqiaY6hmWb2
7r2UYU3FZoCM6viElyb0kKev89pRCiFbjRjIns7qamY18DleD6rLgySBKojAv/RBM2SxuCTLwZO+
qKZIkjmFmA8v6qv3Nvfpa9jovK6TxJ8qkEkD+5gqVHQROmX9uzE1+9mR5Pb4n8C4AI6/X6xJmoW5
ylc2iiUJwVTNy61qy7syCfU4g6WQoM6QYCWDJyFKxlUsGts4yOdOGOdVh6Yb1Y3+SZ8gzV/+2/xz
ToUAZ2kNUF2Gz4I53NBKe9R+ACCPcPzn0ZLWnGrsZXYl+qvQQTMKQnL1OhKzlUsFADGSFCoTjGIe
NBoUURO9LA+mqqbRG9DBGlxzLxOeDCcOQtKS71R0V9mB2+UHWg521lW8Pwh7P0znX1gWvFax6tH9
DZ4H7xrtOy+ZNEDCID7Mg7nkjEmnGlrA2jNvU8IrQtQdRb02D0KrAb/wgJw5jw6pdMSM/fx89+JK
pAb4+iQwHig+S3CPi5sMoJQYSTFF9nPw3uE0x/mrAkk9v64RUwTAyHCZhpuNe2CO8PN0KAx59+hK
HybdHeGK9oBeb0pr64sMHVMHS7E9CjWJvhPvzu7tBq+L/SwxnPLNwB7nv+zH0nv1irV0TqAPEaop
FuNiMgStNfAKFIo7SfI+stg/JysbT71qU9+ZhAjqP4eo4sWW6YcYHZ+/GLafejQeqoyZVVFkduBO
AjE/NNYTtwqhtukgrWCqHh+TXUl2RNqV/GUbIZuPvjmxwXU6fki8ekyoTdZLFZoM8qAzt7MB/z27
bKQ4n4j6JjeHG2b0ZsJH6tYQBLD3wkSf201gty856G+oa8EAsBisvb7x70JEmhYiyIuqb4VgI9lJ
s0rErkwnAsTk/utfhIIS3BN9SsfYqf7R2Od5K7h80TkFWjt+VHn8IKsMcSBLZbbe1gHO794+xfiN
fgd88br/O+5chNBEiUYAgDo/XtYhaRNm6bShEhwWq+zT4pYlYwzQgbc7vPMFP0ft8R0B44WPZbFg
5WMNuNsijV2YTIWsg7yj4kaX2reZenZt2XEKh0lAO9rB0puyhKM96rzEW6x2HbIL+cBUhAxesKbW
bSq7JdHv0k4JPrevPCH4HB5GjkBk0G/yurtCj86IV4MnO8AF+j9ehrJdXIz1Ruo1HDRALqUSd2FN
oWOsu6Oo5CwhNu5dLGqaUIaHU68GaACBtyDE4tm9WkEv+Sqk+Gt5gnMbkPW/gLFKg09iLlEIWqw8
EZtafG/S/AAvqvuYaKoCC4zbSLUKkpxUAduZfRndRfqj1fJJzge5pjr3k+Y4MKV82Cn7QkW2xJB7
5hbSApz8+z/D1e8p6ntLmJ4x32Kj0lbozlyc8ZqKw5y1eoiecs8r6VlfzjwwFDA+Q7Zrw0eOc5eh
wbr8CL1FQ95MSkL8kEcgj3/f0zwb5R68iQEy34FL/JchW+RpULS7vldLfSrdltD/ji8NS3gbwSo9
yFlMm0BYleR9kvH4rLX6VL/aO1q8gp+ITqDVr6N5oYuTih9ssaeJ9TZI2SMHYYW5YQ2bVTAQRzli
lKiDkUJsnYCWlt8p7iQLu81A9xavDhMvDtmZ6o8Is+7XvPvoYQOUgANdXz9JOHKorW20NDNyeXSq
0p+XPR1kZDtHzDraws0cUFPywNdg33Mb/asD+WYB8s3gSMvUqO9HbKe0vVNK22LONEjTfm/xsddI
V3K6prw0hSvAlrDVQ5zlgyhAHhkARhexYeALZfunoodRSjdIg+dHP8assRAAL7Xadk/O9KszWvDg
DzJyYzAOebtBE1GS9IlmCh8rBd1rPVCnVZDdmu0+41/in8sj83eFeQtWmL5a4wKzvlrWq9JcCtDr
Gn8pJOOCLwq89es/IcMTAoxtBSfIHRtgylmCfUUtPL2BGIVOqsGVwr7W1JsOD8EZFtEBp9j3CpVS
+WLXrEZ7dQsF21DUMXn43q1IcYyN5l5GEBaq0/8k8OchMn2X7NCupWnzt664vk6jeBVZgCzppgVJ
X+RiWudRIKVN5TDly2iRUab0PMrGgVWnETt0tycPFfXXH0JuQ/OMmuWwqEsQIAuhGpvR1c8bAu25
yEfP9NSE50pnq4M6MKGmPprSOQHzssCoENWplfH7iQNZz1wV3kd3BRM5LW3Sqx1gUjZmLFbLb5Oo
KWuemBM4qCLXRhcWp3p92G7Glvmp51n8K1+htP2dkiQtuK0df2Ia24hAJvBeKwy+0/lMCRuqcWzE
yp7OfnLdeII5JZQmG+Xy91uXmSmEIeBGfTb/XtQgqr+4aDPSa6S8SXWYFtUd+08TfrMb/4CsUSbv
2j5GHO12CFNpbN2DRY5zV5rH+Bgqh36OSCsr/GCJlA7U4lVqHBrVkotBlXOti7PA/KjClF1aX7tT
CRsDFdaZ3w80fQNBPm42sVdYRm4GYz1GZT1oGjT+hDcwAB9dfGd0WzJ1caagvs560yo/Vls6fVCt
iZ5RCjpLhml7HfqqlCae33iBtTcNLF/POfV/ReR4eE5j2Viw8z/lRLvQYw+DFQIkDdmiop8nREbt
RExACiQSEIlZxwf++Noou1gX5ICc92s+BKA4z1pUBuOv6OzVj+IPiWqPp2G8juRJN79XC/eUans8
yPy7gbZekMMvB+W51tR+FZRUa7VOniCphmvymvdtXljPKOM+AyCBtw38mh2QQpI58oH9OuaGtOeF
WCOTZLszqgaib02EV6Fq8BzY1zrkUU2yJ2LHhLVXqgRbMmJ59ymAs/4JMTJgHDroLI+pyOMJqJ05
L08JOB/qcU/dAc7PzPFhc0UeW/vKUeM9lugJYDv8TBqX4nK4mALTE9J757OQ2L8Etf95MEjPnWGh
Ee9XqGyiZwoN+7ragW9sMT+nquOIkEuee8FOMCdqwmQOUM0TSDnn2yRt0tZBMVgbaFR6Xdl8uACI
u5SBBi8QiQrjDfe3jCJlX/xloxfMakY//iO7nOjmnI0AWBfnRN9CrZC9ivzehC2ruuG19MqV8KgD
97JznRQ05zcN25R4cPQM0KcP1F0dnvvWoVVTQzwUYI21SjrJ2P7drsPr5BmB4dmMUJseP0tJWdFu
uZ3kKmIkOWNOyU90I1ie2rt9CDsSn6O9opkh4eLsQh9WCHZkpkaJXkoEv7m32AcLoYORuZko0HiD
6QHgMPRZhU1ICu5BtvLqZ2J2oAc6VkcOWEhYyALdnag31DZZ/ySeB/YLjOHq4PHu6qAy7RWZQzGc
BBuJyFEV5Dh/Z3j/JmjqvBR4hdlwY14WqnzFh0I43jEtP7+FQZWmUyyq4lh7QqJhdfkcfNWlOd9z
kHdP2O2KK+m9dPHV3NmjpwXBgi+YfzPlV7Aw6fa3chtgCYMmESMAXA3dYUEJ2KCqMP6riS+8CqCM
fIaEtopN3IixJM++WwtZLEnCjf/1z+UEbO9/wNkzJSApCbqJ6yimh8Kip9yn2mlLf0ZYKYHtmvYz
bZTatngOX6Lfqhwk61cH+Jt4udNSRfjRGtLfKKuKSq6/Xmq81desSSJ4BMesl1c78KhjsxVv0NpE
ZPP9N6uiXs05Gt9DDRuB5IjCpimMaJvDPeUXsBnKJ0vNKf2V4zdCrRYGMbjn5qXVrxGmztrhC4a5
oyz+LUkpPZGqhigvUmACG4d6r+WFXHtoIFRQy2un00eIzhO4lRwoWuLOvLZlQ8LByVQzbGp3C4Li
IwPNkL+A6jUOTAkwbWJg3PRMkvWmp0+g14QwUNNDelLlI2d8iNeOBQbQ1qWMmgu3dAYje+3qac6d
Wlg6XPu+LX7tURwTtthPz+Gl1WjlKZx8ze11rWKpL/5NsEtPlvajCq9FVQiNFNDson+8mx7jmsly
FEMnRra09PV5aEnOAzquQgyNq3QDnBrELklEaLo2My0o6encp3tVB0K1FYIqG/15kuQhGxJgK7kQ
GcjNSxiDdHaBuHB56rCak7vagtRGVSCKuXPL8+ETrjjRDDD7QSCv7DYShOYRxpkLw++s77sJrXS5
gt3wSKnJna5U3W5wvHGJdJhxpRoJoyNQsrpnMczDbclDZDA0SB1CCnxgr5ffkOhzZo4UI1BouKjT
+I9ISuZywW4CCQ3XNMb4LQP1DOU+hma6TWSyDNazvOP/GjY4p8ggvyYCqZ9B1Lo+fhzV+OuOcGB1
eUEXwq7aFbkltNyJNNxqK0K9tc8Jq5Mwy1FAX8YEcLtzy9jxm1QT0gPI5FPwPI11RDnxAX9rBLeb
psx9XaWDM3k1WfjKzNNgL5P6Tr3a2YDJ0MB/kCY64J7tn0FdYkHfr94V5ru8/07yC6IAw9UZUZ6q
KxiovM7NBDOw3XPPMZFezQIM5ckn8fJkCBjOiQxAT0Hd4k5cbsaHXbzYHQdm+2Hwoye58IozBofq
CFOTX/tU1SQb1gb6aaj+uScJ1oN0PiKGL0UmAaLkRaWuJ0B80tyjK/EQ/j4FDpSzJKb/+P1z3baJ
Mi4aO1Y6zK+y0SE/5Nw3WZcT9uaLxzd7ZFlN1Ybzf2TKnXJcxrTnwmhm9cr+I1IVqaBX8vAlYBDo
Zeh6WsABE2YDSZMDClEYLHzv++2IywdkipeaygYygrX4CORC9lnd2VQcz9npe9Xyl46/jg7NxP/d
eqsyJNVEe1cc5Eo09CaUzcbiUPultBHNfOD69TCQx7TraatVcmzy+hr+VvAS8y7j9fUCVZgFwV2e
ON8ZBdgpBfSeybmjmg20drPbT38H5PxjcHlDDYsl3J4XiI6X46Ty7e0O0HI/NhqTyzmBTdBsIp9i
WfX/phmgKp315zgK4jNMmINjjhH1Zd+8LGKdMfgk5jV33IoSdSbtwJijGPbR/dLZ6Y/OH0F9nnYW
80bolRxDwX+oAkvrukEIp3YeWOgDXP/Qp9ImR8Ik9fi5PDQFlRm5xw7xUMQ1y5HlQj6emMY3/YDC
BQgQF1fLqSqocoSPDv7kKJu6bhyW+1WI2Hg4JIaD6pRzU13YUZcf2r4gTWc7lFIEKYHhjN9cRDWs
IEBKlBdh7TpwTsLOpYyrGWdAEbzELFvKZwnSZq6EunAhLPYoEZTf3a8hksRk0ZYRF0BswQZ1Kkn3
fp9h47XD25E5LP3Db2DNhIbziQcp5n6+u4n0jU6BsWDOZ334pi8LYiAxBjsC4dx3XoEsB+Dxauoy
zgMpZ8LQW7pn2tIzKdBWRzyA1LDQpTJLTdsS+vsQmGWK88YY23m1THzx2jzlLj+VquOoBZq4jDxW
UGtBiCvg1iUhCYjEZGgIbl2bvWukXukyql44G9PNzkyNR9Unxpm0WgEnoTocpUm4c1tKOJ2Dv4T+
0ztAFoBdotRzst1dWdc2twZVVARKyeejlzyShX3Uq39P8C5n83y/5Qf+uYQxS1tEQYCAoV+GD2xA
swYR1bCj6VS12yKKKkbXnnFHAug2wUFE4vl9DXX4ffGHiURxnFpOWVdCM40RGguT0PBV1kHpz7M8
67PBsZw5+AXv+8DLu0zWxO5OIs3sHOKTDK4wkKFOfi163eIwLWuJbIfB2KFgJVrgvjT7HLy0abXX
R6sU1zMuFtyWEYJ5cz7IZBqIBNLguOHcIa5wLCxLL4SLdSckFn7e3vowkk47P66WbIjA6gnMZ+RD
if0YejiBQ+7Bd+Dk36D2sLZlRPvQSQeyJGE5RiYcoOgA36d1wHZM68uJXQWAP5w98p8FLWNEyfLy
/FcF0Q+/9PMgAv7i+GXyh/UH6WpT7zQ0/mvRUNe9MZKCt7BEjBftcjyc8h2fMv+s4tkMMA3YlR7f
PwchU9+Wf2CLDWDear+Yie1ve/ynhpMmXlsK5MPZxgBPO7KPEjNmsxuOKjZq2DermzQKszPU74cg
nRY9kdpSRq0vBKjoYxyORAshPW1/Te1ombMwzyblUVd1CcsZPVmjt4Q5OHzLE0bfT4l8TeWrzjuA
/7pWhOJ/3FgZxLt7smRf1qtrsnKLdJZbge2WZLdDCfgixLrSAUVaqEVsxUpx7soAdUaU3v8qPEaT
rDfqvK2cTl8ndq6HP7wGJswl+WdeToqbDpN8Btdm7EhZaryhJo+jJHhSRZhxzxgQ3eVbTXBP7+XO
iMdj5s+WLxlVGrGyCqa+PSiNMd8tD38cDKsSHREcFH76/IhrmysJybXzfb0JFHCcwC0viUGIru6B
HQS8gF8HBPei4rMxztupWSc69oF3n36Sb5qN6nrRLY2eZ8ZQbQOlalo7CDGecmpVPq341CsQvBgo
HvMRkppHQz1v4BUqLjWlXXDVuQJsao+eXxy8V97rlcl8i/9dpNkxeeVNUqz+kCdTlclyzZWHTN+g
9TBOJxqJUHiQdOWTfqHzvexBaOrGZ+9zfZX4s+fJb0bMSbpTyQxEMQhiNJAdnILbmgVqi27BtKdD
QCGzscXPaHMyE2hNAa3JPyChmA9DFD2vq2AVMmWFiSFZ8Xo4F9xexVqRUI8WScE0xXFULJM1rvA+
cpHu9RCm98YtWuJ4pC5UuoKU6MDkZyJ3UD7H+JwuJeYC+wu7dMEN5Z78/HtF+03enhN1Ar1LR9Cq
cw2vJMBvMuMBxDRLzdhKEYJ+cvy4iDYhsunDybOafb6XHV54QVL56G2DJVKX3U46e6FFCjWovcc8
Td6e36XJAbIFLLym+Ae/thV4cmMMyvm1SuNzq5L5BXujFbOstMcUaMrxV6RY5frYQx81ytIRtnBW
odoIEnMIuhymGGlnpwUT2kUMzNGjaNBVajCRxIUXbJMPWK4mvipAPHUDEGxNQBTrsegaIwZKN5Ad
nh0ZAGcVBORyDj4YXqFUl4eA//V680D4UMX8nmJ+BiFKSAm0jUGOQO2xe3v00MgyDwXBVGFxbN6Q
eMi0ojob8JIwDDEwHE5hcmEshfTccc+AV3o0eHfvLtodk0XYOUatVWJsaTjSGSDYHH+pcnkk5txI
+lxW9pk7vvGW7fcvg1qxGBMmPmLlkQVY4uigmaPFCWxhx4lNlrZbzRB27qp4N7/vdyksEcNAWE9+
K4LCxmaAdk7/+BvNA5cVvD6zOtCTBuIrs7V1hoS9bPT3D/Vi6estkf53NZ1z9nCx8P1X/Zh4VH3Z
aECYfK8QDPkSTjOqbxjQu3EbC78L/VyRwajsAzKr71s6bT62rYYV8aSWh7KiW2kNIieSa11Uuc0h
t0c6gUqAbMf+8qIsGUweXihQQmVa9P7vinj5U8aodkoGx1r+9IDUpMI2VxhudZ9BgZbLuQ5K3Mv/
l6/+3i61ji9pCKtH7LlbQc4euGEt9mZp65+DGFRdcQCmxgEN2cGXS6APVdcxQdaUWNy0DhFzo/AT
8cOwGvusP5k9/+ymHTzQzuHs9U0/b3K7HDxQ3jZhfMFxpBC5p5KeZblzVUXTifaKdyuWSexTPPY2
MDc4D6Hh6mnc2zUvap5dP4ezdadrNyOUd0N+Ov1uQ/fDw3H0HbHmdhbC9Oru33tGv5TCyei03a0t
MYZzP+0GnB7XMXB6kqW3CzB81fUESY8MioIIxUjE0KBi7W5BhcPtGDvDzhZ3WxPBNLplLFSbpIlq
yr7qBMXET4f4P1ryMMX7t5Ucz2da+l9K9DeTwY9PwWfj/8B7nrGx4amJU5j3/I3T/AQf/8rDJmcC
F51mN9/SAPhGywN8HAcTl7SoMVmiK5SX9zzYTIraapPZxBlaADYXZPfxm3pI8Plc9h/Re5WUdpTL
k9gXgsR3k3kHAcLWUe+bppHZLP48NMzZJEyM6bekV8XkaME6+Kx4uXs8T+qsT6p4RtZxR948YKwF
4M+ljmYmgZy9nI0QSonqPwewiecuv4PgiIizWqW2ddWBPcKLtM8/pBSNrP8PjOrCksy0hQnEDk4o
PlUzHFFEjbTR4Q+G6QOI0hX++5nqdQYOauzUl15evwsnX40Hn/wZQvb7v6ZcUyZ4muhj9Fial4kx
J3g5wBaypIbIKmOgqCl6NA/zTbGdZ0qM5vMmoQGxlyv/g7PctiHHL9BNjt3GoFwdys8sMulyNL7q
1MgRoAF3zfSQ3x/t7NJisE1BCZ83p7EkkfT5Ld/fceUJi2Y2XylCRvzqy/UYy4NHdGm/SjTY0xWY
K81shuYCJ5qWpUo9OKeDSQol5j0zoUyz/Hv8dxtEbKV+gEJBppe0yVkv/t50Bbe1YJCQSNhrnA0z
B6ZnxOeJO/KGkMrCr/mp1dTTL79HlYeGJluV6xAdRhO+FC15YPMOe+hwOXvcFnjA8/GPHkLgl4YU
dVwR/knLD4qjOyiuExxXoDNuP8ZQoqCRZTgKIUc6ga3C6maCgechSnDSLRnLtNR0LFeaB3I0bKde
ddf8RlBMKWaWBkY1ECrNmzw6LQqXhceeoug7qoWrvYtszZM5mKvECkOP1Ds9LgHnkArBBNilKAoG
NI+8X4R5zHNjYY06xYEHsRVSSHD53VE2+ndNlq2/MAJNu1+6TlnGNLR58jkxYKi8EtN7YDlF3KlW
Uwxo/bbEuoCQr9bcFBeXaOQAR7zDBLb0wS6Qw4FqoBfSBe9O+KICA94s7MPFVxvgC94AXuLYzNUO
uGdQKV/ITP/CNpK6qSgxI20Q4/Z9AilyDRgNsJlBa7f1jrPst8mbxg08dl/OSQPvJHojnavtyBcQ
LExeBdc7eOXwfPfn82hWb8KHQOk/5awMAP5P+uJfVgt+OgUaln5tDOeXvI+9PaFXgW1Xuhs3ysAV
CqXLyiDgfFCaywS1IxV7QU8pGxfzQ9xwp/eBZGJB1FK72C1GfXqzAO7Exzr014ZCXgL0xhI5l0mn
2/wmBfFzlDTaf0Hp7Ji5ohpUrEIo+3tCWAyAxZ7SCMHU
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
