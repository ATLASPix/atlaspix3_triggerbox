library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity triggering_FSM is
    Port ( clk : std_logic;
           rst : std_logic;
           trigger_re : in STD_LOGIC;
           trigger_output : out STD_LOGIC;
           delay : in integer;
           pulse_width : in integer;
           busy : out STD_LOGIC);
end triggering_FSM;

architecture Behavioral of triggering_FSM is

type state_type is (IDLE,TRIGGERED,SENDINGTRG);
signal state,next_state : state_type;

signal pulse_cnt : integer :=0;
signal delay_cnt : integer :=0;


begin

--SYNC_PROC  : process(clk)
--begin
--    if rising_edge(clk) then 
--        if(rst = '1') then 
--            state <= IDLE;
--        else
--            state <= state;
--        end if;
--    end if;
--end process;


OUTPUT_DECODE  : process(clk)
begin
    if rising_edge(clk) then

        case state is 
            when IDLE =>
                busy <= '0';
                pulse_cnt <= 0;
                trigger_output <= '0';  
                delay_cnt <= 0 ;
    
            when TRIGGERED =>
                busy <= '1';
                pulse_cnt <= 0 ;
                trigger_output <= '0';
                delay_cnt <= delay_cnt+1;

            when SENDINGTRG =>     
                busy <= '1';
                delay_cnt <= 0 ;
                trigger_output <= '1';
                pulse_cnt <= pulse_cnt +1;
            when others => 
                busy <= '0';
                pulse_cnt <= 0;
                trigger_output <= '0';  
                delay_cnt <= 0 ;            
        end case;
    end if;
end process;

NEXT_STATE_DECODE  : process(clk)
begin
    if rising_edge(clk) then
    
    case state is 
        when IDLE =>
            if(trigger_re = '1') then 
                state <= TRIGGERED;
            else 
                state <= IDLE;
            end if;
        when TRIGGERED => 
                if(delay_cnt = delay) then
                    state <= SENDINGTRG;
                else
                    state <= TRIGGERED;
                end if;
        when SENDINGTRG =>                 
                if(pulse_cnt = pulse_width) then
                    state <= IDLE;
                else
                    state <= SENDINGTRG;                  
                end if;
        when others =>
                state <= IDLE;
        end case;        
    end if;
end process;





end Behavioral;
