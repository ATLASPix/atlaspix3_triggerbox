# ATLASPix3 Trigger Box 

This IP handle the triggering of the ATLASPix3 when using the external trigger pin. A trigger is received and re-emmited to ATLASPix3 after a delay of X 25 ns units for a duration of Y 25 ns units.


## Registers

- **Register 0** (0x0) DELAY, 32 bits : Delay in clock unit of the emmited trigger
- **Register  1** (0x4) PULSE_WIDTH, 32 bits : Width of the trigger pulse sent to ATLASPix3